import React,{Component} from 'react';

  
  class RentFlat extends Component {
    state = {}
  
  
    render() {
      
      return (
        <div className="container">
        <h3>POST YOUR AD</h3>
        <div class="bordered">
            <form action="#" className="white" >
                <h5>INCLUDE SOME DETAILS</h5>
                
                <div>
                    <label class="blue-text text-darken-3">Type</label>    
                    <p>
                        <label>
                            <input class="with-gap" name="Type" type="radio" />
                            <span>Apartment</span>
                        </label>
                        <label>
                            <input class="with-gap" name="Type" type="radio"  />
                            <span>Bungalow</span>
                        </label>
                    </p>
                </div>
                <div>
                    <label>Bedrooms</label>    
                    <p>
                        <label>
                            <input class="with-gap" name="bedrooms" type="radio" />
                            <span>1</span>
                        </label>
                        <label>
                            <input class="with-gap" name="bedrooms" type="radio"  />
                            <span>2</span>
                        </label>
                        <label>
                            <input class="with-gap" name="bedrooms" type="radio"  />
                            <span>3</span>
                        </label>
                        <label>
                            <input class="with-gap" name="bedrooms" type="radio"  />
                            <span>4</span>
                        </label>
                        <label>
                            <input class="with-gap" name="bedrooms" type="radio"  />
                            <span>5</span>
                        </label>
                    </p>
                </div>
                <div>
                    <label>Bathrooms</label>    
                    <p>
                        <label>
                            <input class="with-gap" name="bathrooms" type="radio" />
                            <span>1</span>
                        </label>
                        <label>
                            <input class="with-gap" name="bathrooms" type="radio"  />
                            <span>2</span>
                        </label>
                        <label>
                            <input class="with-gap" name="bathrooms" type="radio"  />
                            <span>3</span>
                        </label>
                        <label>
                            <input class="with-gap" name="bathrooms" type="radio"  />
                            <span>4</span>
                        </label>
                        <label>
                            <input class="with-gap" name="bathrooms" type="radio"  />
                            <span>5</span>
                        </label>
                    </p>
                </div>
               <div>
                   
                        <label>Furnishing</label>
                    <p>    
                        <label>
                            <input class="with-gap" name="furnishing" type="radio"  />
                            <span>Furnished</span>
                        </label>
                        <label>
                            <input class="with-gap" name="furnishing" type="radio"  />
                            <span>Semi-Furnished</span>
                        </label>
                        <label>
                            <input class="with-gap" name="furnishing" type="radio"  />
                            <span>Unfurnished</span>
                        </label>
                   </p>  
               </div>
               <div>
                   
                        <label>Listed By</label>
                    <p>    
                        <label>
                            <input class="with-gap" name="listedby" type="radio"  />
                            <span>Builder</span>
                        </label>
                        <label>
                            <input class="with-gap" name="listedby" type="radio"  />
                            <span>Dealer</span>
                        </label>
                        <label>
                            <input class="with-gap" name="listedby" type="radio"  />
                            <span>Seller</span>
                        </label>
                   </p>
               </div>

               <div>
                   <p>
                       <label>Super Builtup area (ft²) *</label>
                       <input  type="text" name="builtup-area"/>
                   </p>
               </div>
               <div>
                   <p>
                       <label>Carpet Area</label>
                       <input type="text" />
                   </p>
               </div>
               <div>
                   <p>
                        <label>Bachelors Allowed</label>
                        <div class="switch">
                            <label>
                            No
                            <input type="checkbox" name="bachelors"/>
                            <span class="lever"></span>
                            Yes
                            </label>
                        </div>
                   </p>
               </div>
            
               <div>
                   <p>
                       <label>Maintenance Monthly</label>
                       <input type="text" name="maintainence"/>
                   </p>
               </div>

               <div>
                   <p>
                       <label>Total floors</label>
                       <input type="text" name="total-floors" />
                   </p>
               </div>

               <div>
                   <p>
                       <label>Floor No</label>
                       <input type="text" />
                   </p>
               </div>
               <div>
                   <p>
                       <label>Description</label>
                       <input type="text" />
                   </p>
               </div>
                
                <div>
                    <button class="btn waves-effect waves-light" type="submit" name="action">Post
                        <i class="material-icons right">cloud</i>
                    </button>
                </div>
                
            </form>
            </div>    
        </div>
      );
      }
    }


export default RentFlat;