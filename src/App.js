import React, { Component } from 'react';
import { BrowserRouter , Switch , Route } from 'react-router-dom';
import MainPage from './components/landing/MainPage';
import RentFlat from './components/seller/RentFlat';

class App extends Component {
  render(){
    return (
      <BrowserRouter>
        <div className="App">
          <Switch>
            <Route exact path='/' component={MainPage} />
            <Route path='/posts' component={RentFlat} />
          </Switch>
        </div>
      </BrowserRouter>
    );
  }
  
}

export default App;
